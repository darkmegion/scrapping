const vanillaPuppeteer = require('puppeteer');
const { addExtra } = require('puppeteer-extra');
const Stealth = require('puppeteer-extra-plugin-stealth');
const { Cluster } = require("puppeteer-cluster");

const fs = require('fs');
const path = require('path');
const privates = require('./privates');

const puppeteer = addExtra(vanillaPuppeteer)
	puppeteer.use(Stealth());		

if(!privates.START){
	console.log('No se procesa');
	return;
}

const result = [];
const users = [];
for (let i = 1; i < 21; i++) {
	users.push(privates.G1[i]);
}

( async () => {
	const cluster = await Cluster.launch({
		concurrency: Cluster.CONCURRENCY_CONTEXT,
		maxConcurrency: 20,
		monitor: false,
		timeout: 500000,
		puppeteer,
		puppeteerOptions: {
			headless: true
		}
	});

	cluster.on("taskerror", (err, data) => {
		console.log("Error crawling" + JSON.stringify(data) + ":" + "err.message");
	});
	
	await cluster.task(async ({ page, data, worker }) => {
		console.log(data);
		await page.setDefaultNavigationTimeout(0); 
	
		const requests = {
			checkConditions: 0,
			enrolCourse: 0,
			checkedConditions: 0
		}

		

		await page.setViewport({width: 1080, height: 700});
		await page.goto('https://miportal-stress.cientifica.edu.pe/admin');

		/* INICIO DE SESION ADMIN */
		await page.waitForTimeout(2000);
		const selectorInputUserOutlook = 'input[formcontrolname=email]';
		await page.waitForSelector(selectorInputUserOutlook);
		await page.type(selectorInputUserOutlook, data[1]);
		await page.waitForTimeout(500);
		const selectorInputPassOutlook = 'input[formcontrolname=password]';
		await page.waitForSelector(selectorInputPassOutlook);
		await page.type(selectorInputPassOutlook, '123456');
		await page.waitForTimeout(500);
		await page.keyboard.press('Enter');

		console.log('que');

		await page.waitForTimeout(1500);
			/* ENTRAR A MIPORTAL COMO ALUMNO */
			const selectorLinkTramites = '.row.d-flex.justify-content-center.mb-3.px-5 button';
			const pullovers = await page.$$(selectorLinkTramites);
			if(pullovers.length === 0) console.error('No se encontró botón de MIPORTAL');
			console.log(pullovers.length);
			await pullovers[0].click();
			await page.waitForTimeout(1000);
			const selectorInputCodeStudent = 'input[name=code]';
			await page.waitForSelector(selectorInputCodeStudent);
			await page.type(selectorInputCodeStudent, data[0]);
			await page.waitForTimeout(1000);
			const selectorButtonSearchStuent = '.smallModall button';
			const pulloversBtnSearchStudent = await page.$$(selectorButtonSearchStuent);
			if(pulloversBtnSearchStudent.length === 0) console.error('No se encontró botón de buscar');
			console.log(pulloversBtnSearchStudent.length);
			await pulloversBtnSearchStudent[0].click();
			console.log('ya pe')

		async function accessWithStudent() {
			await page.waitForTimeout(1500);
			/* ENTRAR A MIPORTAL COMO ALUMNO */
			const selectorLinkTramites = '.row.d-flex.justify-content-center.mb-3.px-5 button';
			const pullovers = await page.$$(selectorLinkTramites);
			if(pullovers.length === 0) console.error('No se encontró botón de MIPORTAL');
			console.log(pullovers.length);
			await pullovers[0].click();
			await page.waitForTimeout(1000);
			const selectorInputCodeStudent = 'input[name=code]';
			await page.waitForSelector(selectorInputCodeStudent);
			await page.type(selectorInputCodeStudent, data[0]);
			await page.waitForTimeout(1000);
			const selectorButtonSearchStuent = '.smallModall button';
			const pulloversBtnSearchStudent = await page.$$(selectorButtonSearchStuent);
			if(pulloversBtnSearchStudent.length === 0) console.error('No se encontró botón de buscar');
			console.log(pulloversBtnSearchStudent.length);
			await pulloversBtnSearchStudent[0].click();
		}

		async function selectStudent(){
			await page.waitForTimeout(1000);
			const selectorButtonSelectStuent = '.smallModall .row.d-flex button';
			const pulloversBtnSelectStudent = await page.$$(selectorButtonSelectStuent);
			if(pulloversBtnSelectStudent.length === 0) console.error('No se encontró botón de seleccionar');
			console.log(pulloversBtnSelectStudent.length);
			await pulloversBtnSelectStudent[0].click();
		}

		async function checkedConditions(){
			requests.checkedConditions++;
			if(requests.checkedConditions < 3){
				saveConditions();
			}
			else {
				enterEnrol();
			}
		}

		async function accessConditions(){
			const url = await page.url();
			console.log(url);
			if(url.indexOf('estudiante/inicio') === -1) return; 
			await page.waitForTimeout(3000);
			/* CONDICIONES ACADEMICAS */
			checkedConditions();	
		}

		async function saveConditions(){
			await page.waitForTimeout(2000);
			const selectorButtonConditions = '.text-center.mx-1.pointer.ng-star-inserted .bg-red.text-white.p-2.text-s.ng-star-inserted';
			const pulloversBtnConditions = await page.$$(selectorButtonConditions);
			if(pulloversBtnConditions.length === 0) {
				console.error('Aceptó todas las condiciones');
				enterEnrol();
			} else {
				await pulloversBtnConditions[0].click();
				await page.waitForTimeout(1000);
				const selectorButtonAccept = '.mediumModal button';
				const pulloversBtnAccept = await page.$$(selectorButtonAccept);
				if(pulloversBtnAccept.length === 0) console.error('No se encontró botón aceptar');
				else await pulloversBtnAccept[0].click();
			}
		}

		async function enterEnrol(){
			await page.waitForTimeout(2000);
			/* REDIRECCION A MATRICULA */
			const selectorLinkMenu = 'app-menu-items .sidebar-menu .menu';
			const pulloversLinkMenu = await page.$$(selectorLinkMenu);
			if(pulloversLinkMenu.length === 0) console.error('No se encontró menú');
			else {
				await pulloversLinkMenu[6].click();
				await page.waitForTimeout(2000);
				const selectorLinkSubMenu = 'app-menu-items .sidebar-menu .item-sub[style="display: block;"] li';
				const pulloversLinkSubMenu = await page.$$(selectorLinkSubMenu);
				if(pulloversLinkSubMenu.length === 0) console.error('No se encontró submenú de matrícula');
				else await pulloversLinkSubMenu[1].click();
			}
		}

		async function checkedCourse(){
			requests.enrolCourse++;
			if(requests.enrolCourse == 1){
				enrolCourses('001002');
			}
			else if(requests.enrolCourse == 2){
				enrolCourses('001005');
			}
			else if(requests.enrolCourse == 3){
				enrolCourses('667227');
			}
			else if(requests.enrolCourse == 4){
				preFinishEnrol();
			}
		}

		async function enrolCourses(courseID){
			await page.waitForTimeout(3000);
			const selectorCourse1 = 'input[id="' + courseID + '"]';
			const pulloversCourse1 = await page.$$(selectorCourse1);
			if(pulloversCourse1.length === 0){
				console.error('Curso ' + courseID + ' ya matriculado');
				checkedCourse();
			}
			else {
				await pulloversCourse1[0].click();
			}
		}

		async function saveEnrolCourse(){
			await page.waitForTimeout(2000);
			const options3 = await page.$$eval('ngx-smart-modal[identifier="scheduleSelection"] label', options => {
				options = options.map((option, index) => option.textContent.indexOf('PRA') !== -1?index:-1);
				return options = options.filter(i => i != -1);
			});
			console.log(options3);

			await page.waitForTimeout(1000);
			while((await checkisntEmpty('ngx-smart-modal[identifier="scheduleSelection"]')) && !(await checkisntEmpty('backdrop full-screen'))){
				const selectorSChedule1 = 'ngx-smart-modal[identifier="scheduleSelection"] input';
				const pulloversSchedule1 = await page.$$(selectorSChedule1);
				if(pulloversSchedule1.length === 0) console.error('No se encontró un horario');
				else {
					let numSChedule = Math.floor(Math.random()*(options3.length-1));
					await pulloversSchedule1[options3[numSChedule]].click();

					await page.waitForTimeout(2000);
					const selectorEnrol1 = 'ngx-smart-modal[identifier="scheduleSelection"] button.btn-orange';
					const pulloversEmrol1 = await page.$$(selectorEnrol1);
					console.log(pulloversEmrol1.length);
					console.log("Boton Matricular");
					if(pulloversEmrol1.length === 0) console.error('No se encontró botón matricular');
					else await pulloversEmrol1[0].click();

					options3.splice(numSChedule, 1);
				}
				await page.waitForTimeout(500);
			}		
		}

		async function preFinishEnrol(){
			/* CAPTURAR FOTO */
			await page.waitForTimeout(2000);
			const selectorEnrol1 = '.buttons button';
			const pulloversEmrol1 = await page.$$(selectorEnrol1);
			if(pulloversEmrol1.length === 0) console.error('No se encontró link Cursos Matriculados');
			else await pulloversEmrol1[1].click();
		}

		async function finishEnrol(){
			await page.waitForTimeout(1000);
			const capture = await page.screenshot({fullPage: true, encoding: 'base64'});
			fs.writeFile(path.resolve(__dirname, './public/image.png'), capture, 'base64', function(err) {
				if (err) throw err;
				// bot.sendPhoto(-856194584, './public/image.png');
			});
			const dtEnd = Date.now();
			console.log(dtEnd - dtInit);
			console.log('cerramos');
			browser.close();
		}

		async function checkisntEmpty(selectorSChedule1){
			const pulloversSchedule1 = await page.$$(selectorSChedule1);
			return pulloversSchedule1.length != 0;
		}
	});

	for (let i = 0; i < 20; i++) {
		cluster.queue(users[i]);
	}
})();